<?php

function iut_wp_enqueue_styles() {

 $parenthandle = 'twentynineteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 $theme = wp_get_theme();

 wp_enqueue_style( $parenthandle,
 get_template_directory_uri() . '/style.css',
 array(), // If the parent theme code has a dependency, copy it to here.
 $theme->parent()->get( 'Version' )
 );


 wp_enqueue_style( 'iut-style',
 get_stylesheet_uri(),
 array( $parenthandle ),
 $theme->get( 'Version' ) // This only works if you have Version defined in the style header.
 );
}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_styles' );

function iut_register_post_type_recette(){
    register_post_type(
        "recette",
        array(
            "labels" => array(
                'name' => 'Recettes',
                'singular_name' => 'Recettes'
            ),
            'public' => true,
            'publicity_queryable' => true,
            'show_in_rest' => true,
            'hierarchical' => true,
            'supports' => array('title', 'editor', 'thumbail','page-attributes'),
            'has_archive' => 'recettes',
            'rewrite' => array('slug' => 'recipe'),

        )
        );
}

add_action( 'init', 'iut_register_post_type_recette',10);

function iut_add_meta_boxes_recette(){

    add_meta_box(
        'iut_mbox_recette',
        'Info complémentaires',
        'iut_mbox_recette_content',
        'recette'
    );
}

add_action('add_meta_boxes', 'iut_add_meta_boxes_recette');


function iut_mbox_recette_content( $post ){

    $iut_ingredient = get_post_meta($post -> ID, 'iut-ingredient', true);

    echo '<p>';
    echo '<label for = "">';
    echo 'Ingrédients : ';
    echo '<textarea id = "iut-ingredient" name = "iut-ingredient" value = "'.$iut_ingredient.'"></textarea>';
    echo '</label>';
    echo '</p>';

    $iut_temps_de_cuisson = get_post_meta($post -> ID, 'iut-ingredient', true);

    echo '<p>';
    echo '<label for = "">';
    echo 'Temps de cuisson  : ';
    echo '<textarea id = "iut_temps_de_cuisson" name = "iut_temps_de_cuisson" value = "'.$iut_temps_de_cuisson.'"></textarea>';
    echo '</label>';
    echo '</p>';
}


function  iut_save_post( $post_ID){

    if( isset ($_POST['iut-ingredient']) && !empty( $_POST['iut-ingredient']) ){
        update_post_meta($post_ID, 'iut-ingredient', sanitize_text_field($_POST['iut-ingredient']));
    }

}



add_action('save_post', 'iut_save_post');

